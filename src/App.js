import Info from "./components/Info";

function App() {
  return (
    <div>
      <Info firstName="Minh" lastName="Vu Dang" favNumber={25}/>
    </div>
  );
}

export default App;
